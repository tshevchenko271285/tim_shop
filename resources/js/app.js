/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));



Vue.component('order-item', require('./components/OrderItemComponent.vue').default);
Vue.component('order-items', require('./components/OrderItemsComponent.vue').default);
Vue.component('shop-cart', require('./components/CartComponent.vue').default);
Vue.component('header-cart-button', require('./components/CartHeaderButtonComponent').default);

Vue.component('orders-list', require('./components/orders/OrdersComponent.vue').default);
Vue.component('order-list-item', require('./components/orders/OrderListItem.vue').default);
Vue.component('order-list-item-status', require('./components/orders/OrderStatusComponent').default);
Vue.component('orders-pagination', require('./components/orders/OrdersPaginationComponent').default);
Vue.component('orders-status-filter', require('./components/orders/OrderFilterComponent').default);

Vue.component('customer-details-form', require('./components/customerDetailsForm.vue').default);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('tim-messages', require('./components/messages/TimMessagesComponent.vue').default);
Vue.component('tim-message', require('./components/messages/TimMessageComponent.vue').default);
Vue.component('tim-write-message', require('./components/messages/TimWriteMessageComponent.vue').default);

/*
**  Admin Section
 */
// Categories
Vue.component('create-category-form', require('./components/admin/categories/CategoryFormComponent').default);
Vue.component('category-list', require('./components/admin/categories/CategoryListComponent').default);
Vue.component('category-child-items', require('./components/admin/categories/CategoryListItemComponent').default);
Vue.component('category-modal-remove', require('./components/admin/categories/CategoryModalRemoveComponent').default);

// Products
Vue.component('admin-products', require('./components/admin/products/ProductsComponent').default);
Vue.component('admin-product', require('./components/admin/products/ProductComponent').default);
Vue.component('admin-product-edit', require('./components/admin/products/ProductEditComponent').default);
Vue.component('admin-product-gallery', require('./components/admin/products/ProductGalleryComponent').default);
Vue.component('admin-product-categories', require('./components/admin/products/ProductCategoriesComponent').default);

import ButtonBuy from './mixins/buttonBuyMixin';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import store from './store';

import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

export const eventEmiter = new Vue();

const app = new Vue({
    el: '#app',
    store,
    mixins: [ButtonBuy],
    async mounted() {
        await this.$store.dispatch('getCartItems');
    },
});
window.x = app;
require('./custom');

