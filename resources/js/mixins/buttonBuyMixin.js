export default {
    methods: {
        buyProduct(event) {
            let button = event.target,
                url = button.getAttribute('data-add-cart-url');
            axios({
                method: 'post',
                url: url,
                data: {}
            }).then( res => this.$store.dispatch('getCartItems') );
        }
    }
}
