function createErrorModal(errors) {
    let $list = '';
    for(let item in errors ) {
        for( let error of errors[item] ) {
            $list += '<li class="error-modal_list">'+error+'</li>';
        }
    }
    let $newModal = $(`
        <div class="error-modal">
            <div class="error-modal_substrate"></div>
            <div class="error-modal_window">
                <div class="error-modal_close">
                    <i class="far fa-times-circle"></i>
                </div>
                <div class="error-modal_content">${$list}</div>
                <div class="error-modal_buttons">
                    <div class="button button__gray error-modal_button">OK</div>
                </div>
            </div>
        </div>
    `);
    $('body').append($newModal);
    $newModal.fadeIn(100);
    $(document).on('click', '.error-modal_close, .error-modal_button', function(){
        $newModal.fadeOut(100);
    });
}

window.timScroll = function(to){
    let y = window.scrollY,
        timer;
    timer = setInterval(()=>{
        if( y <= to ) {
            y = y+5;
            window.scrollTo(0, y);
            if( y >= to )
                clearTimeout(timer);
        } else {
            y = y-5;
            window.scrollTo(0, y);
            if( y <= to )
                clearTimeout(timer)
        }
    }, 1)
};


$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        items: 4,
        margin: 15
    });
});
$(document).on('click', '#addNewImage', function(){
    $('#addImageField').trigger('click');
});
$(document).on('change', '#addImageField', function () {
    var $form = $(this).parents('form'),
        url = $form.attr('action'),
        formData = new FormData($form[0]);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false
    }).done(function (data) {
        $('.owl-carousel').trigger('add.owl.carousel', [data]).trigger('refresh.owl.carousel');
    }).fail(function(jqXHR, textStatus, errorThrown) {
        let errors;
        if( jqXHR.status == 422) {
            errors = JSON.parse(jqXHR.responseText);
            createErrorModal(errors.errors);
        }
    });
});

$(function(){
    $(document).on('click', '.product-gallery_item-action__thumbnail', function(){
        let $this = $(this),
            $item = $this.parents('.product-gallery_item'),
            imageId = $item.attr('data-image-id'),
            imageUrl = $item.find('img.product-gallery_item-image').attr('src'),
            routeUrl = $this.parents('.product-gallery').attr('data-setthumbnail-url');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: routeUrl,
            type: "POST",
            data: {
                image_id : imageId
            },
        }).done(function (res) {
           if( res == 'true' ) {
               $('.product-detail_thumbnail > img').attr('src', imageUrl);
           }
        });
    });
});

$(function(){
    $(document).on('click', '.product-gallery_item-action__delete', function(){
        let $this = $(this),
            carouselItemIndex = $this.parents('.owl-item').index(),
            thumbnailContainer = $('.product-edit_thumbnail'),
            thumbnail = thumbnailContainer.find('img'),
            routeUrl = $this.attr('data-delete-image');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: routeUrl,
            type: "DELETE",
        }).done(function (res) {
            if( res == 'true' ) {
                var indexToRemove = 2;
                $(".owl-carousel").trigger('remove.owl.carousel', [carouselItemIndex]).trigger('refresh.owl.carousel');
            } else if( res == 'thumbnail' ) {
                let defaultThumbnail = thumbnailContainer.attr('data-default-thumbnail');
                thumbnail.attr('src', defaultThumbnail);
                $(".owl-carousel").trigger('remove.owl.carousel', [carouselItemIndex]).trigger('refresh.owl.carousel');
            }
        });
    });
});

$(function(){
    let $editButton = $('.product-detail_action-edit'),
        $saveButton = $('.product-detail_action-save');
    $editButton.on('click', function(e){
        let $this = $(this),
            $contentContainer = $('.product-detail_content'),
            $title = $contentContainer.find('.product-detail_content-title'),
            $titleText = $title.text(),
            $description = $contentContainer.find('.product-detail_content-description-text'),
            $descriptionText = $description.html(),
            $price = $contentContainer.find('.product-detail_content-price'),
            $priceText = $price.text();

        $contentContainer.addClass('product-detail_content__edit');
        $title.html(`<input type="text" id="productTitle" value="${$titleText}" />`);
        $description.html($descriptionText).summernote({height: 200});
        $price.html(`<input type="number" step="0.01" id="productPrice" value="${$priceText}" />`);

        $this.toggleClass('product-detail_action__hidden');
        $saveButton.toggleClass('product-detail_action__hidden');
    });


    $saveButton.on('click', function(e){
        let $this = $(this),
            $contentContainer = $('.product-detail_content'),
            routeUrl = $contentContainer.attr('data-update-url'),
            $title = $contentContainer.find('#productTitle'),
            $titleVal = $title.val(),
            $description = $contentContainer.find('.product-detail_content-description-text'),
            $descriptionVal = $description.summernote('code'),
            $price = $contentContainer.find('#productPrice'),
            $priceVal = $price.val();;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                title: $titleVal,
                description: $descriptionVal,
                price: $priceVal,
            },
            url: routeUrl,
            type: "PUT",
        }).done(function (res) {
            $('.product-detail_content-title').html($titleVal);
            $description.summernote('destroy');
            $('.product-detail_content-price').html($priceVal);

            $('.product-detail_content').removeClass('product-detail_content__edit');
            $this.toggleClass('product-detail_action__hidden');
            $editButton.toggleClass('product-detail_action__hidden');
        }).fail(function(jqXHR, textStatus, errorThrown) {
            let errors;
            if( jqXHR.status == 422) {
                errors = JSON.parse(jqXHR.responseText);
                createErrorModal(errors.errors);
            }
        });
    });

    $(document).on('click', '.product-detail_button-remove', function(){
        let $this = $(this),
            routeUrl = $this.attr('data-destroy-product-url'),
            $confirmModal = $(`
                <div class="modal-product-remove">
                    <div class="modal-product-remove_substrate"></div>
                    <div class="modal-product-remove_window">
                        <div class="modal-product-remove_close"><i class="far fa-times-circle"></i></div>
                        <div class="modal-product-remove_content">
                            Подтвердить удаление
                        </div>
                        <div class="modal-product-remove_buttons">
                            <div class="button button__gray modal-product-remove_button modal-product-remove_button__confirm">Удалить</div>
                            <div class="button button__gray modal-product-remove_button modal-product-remove_button__cancel">Отмена</div>
                        </div>
                    </div>
                </div>
            `);
        $('body').append($confirmModal);
        $('.modal-product-remove_substrate, .modal-product-remove_close, .modal-product-remove_button__cancel').on('click', function(){
             $('.modal-product-remove').remove();
        });
        $(document).on('click', '.modal-product-remove_button__confirm', function(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: routeUrl,
                type: "DELETE",
            }).done(function (res) {
                // console.log(res);
                window.location.href = res;
            }).fail(function(jqXHR, textStatus, errorThrown) {
                let errors;
                if( jqXHR.status == 422) {
                    errors = JSON.parse(jqXHR.responseText);
                    createErrorModal(errors.errors);
                }
            });
        });
    });

});

// $(function(){
//     if( !$('#newPostContainer').length ) return;
//    // Отделения новой почты
//     let url = 'https://api.novaposhta.ua/v2.0/json/',
//         apiKey = '228443b40d64efba8bc2502edc4e7b67';
//     fetch(url, {
//         method: "POST",
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({
//             "apiKey": apiKey,
//             "modelName": "Address",
//             "calledMethod": "getAreas",
//             "methodProperties": {}
//         })
//     }).then(
//         (response) => response.json()
//     ).then( (response) => {
//         let areas = response.data;
//         if( areas.length ) {
//             createAreasSelect(areas);
//         }
//     });
//
//
//     function createAreasSelect(areas) {
//         if( !areas.length ) return;
//
//         let $newPostContainer = $('#newPostContainer');
//
//         let $select = $(`
//             <select class="form-control" id="newPostAreasSelect">
//                 <option value="0">Выберите область</option>
//             </select>
//         `);
//
//         let $selectContainer = $(`
//             <div class="form-group" id="newPostAreas">
//                 <label for="newPostAreasSelect">Область</label>
//             </div>
//         `);
//
//         for( let area of areas ) {
//             // console.log(area);
//        // <option value="${area.AreasCenter}">${area.Description}</option>
//             $select.append($(`
// <option value="${area.Ref}">${area.Description}</option>
//
//             `));
//         }
//
//         if( $newPostContainer.find('#newPostAreas').length ){
//             $newPostContainer.find('#newPostAreas').remove();
//             $(document).off('change.newPostSelectAreas');
//         }
//
//         $selectContainer.append($select);
//         $newPostContainer.append($selectContainer);
//         $(document).on('change.newPostSelectAreas', '#newPostAreasSelect', function(){
//             let $this = $(this),
//                 areaId = $this.val();
//                 // areaId = $this.find('option:selected').text();
//             getNewPostCitiesByAreaId(areaId);
//         });
//     }
//
//     function getNewPostCitiesByAreaId(areaId) {
//         console.log(areaId);
//         fetch(url, {
//             method: "POST",
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({
//                 "apiKey": apiKey,
//                 "modelName": "Address",
//                 "calledMethod": "getCities",
//                 // "methodProperties": {
//                 //     "FindByString": areaId
//                 // },
//
//                 // "modelName": "AddressGeneral",
//                 // "calledMethod": "getWarehouses",
//                 // "methodProperties": {
//                 //     "Language": "ru",
//                 //     'CityRef': areaId
//                 // },
//
//                 // "modelName": "AddressGeneral",
//                 // "calledMethod": "getSettlements",
//                 // "methodProperties": {
//                 //     // "AreaRef": areaId,
//                 //     // // "Ref": areaId,
//                 //     // // "RegionRef": areaId,
//                 //     // "Page": "1"
//                 // },
//
//             })
//         }).then(
//             (response) => response.json()
//         ).then( (response) => {
//             let test = [];
//             console.log(response);
//             let cities = response.data;
//             if( cities.length ) {
//                 console.log(cities);
//                 cities = cities.filter(item => {
//                     if( item.Area == areaId ) {
//                         test.push()
//                         return true;
//                     }
//                 });
//                 console.log(cities);
//             }
//         });
//     }
//
// });
