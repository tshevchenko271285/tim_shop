export default {
    state: {
        categories: [],
        editableCategory: {
            id: null,
            parent_id: null,
            title: '',
            // slug: '',
        }
    },
    actions: {
        changeEditableCategory(context, payload) {
            let url, method;
            if( payload.id ) {
                url = '/admin/categories/' + payload.id;
                method = 'PUT';
            } else {
                url = '/admin/categories';
                method = 'POST';
            }
            axios({
                method,
                url,
                data: {
                    parent_id: payload.parent_id,
                    title: payload.title,
                }
            }).then(function (response) {
                context.commit('resetEditableCategory', payload);
                context.dispatch('getCategories');
            }).catch(error=>{
                console.error('Validation Error:',error.response);
            });
        },
        getCategories(context) {
            axios({
                method: 'POST',
                url: '/admin/categories/getCategories',
            })
            .then(response => {
                context.commit('setCategories', response.data)
            })
            .catch(error=>{
                console.error('Get Categories Error:',error.response);
            });
        },
        validateUniqueCategoryByTitle(context) {
            let validationCategory = context.getters.editableCategory;
            return axios({
                method: 'POST',
                url: '/admin/categories/validateUniqueCategoryByTitle',
                data: {
                    id: validationCategory.id,
                    title: validationCategory.title
                }
            })
            .then(response => {
                return response.data;
            });
        },
        removeCategory(context, category) {
            axios({
                method: 'DELETE',
                url: '/admin/categories/'+category.id,
            })
            .then(response => {
                context.dispatch('getCategories');
            });
        }
    },
    mutations: {
        changeEditableCategory(state, payload) {
            state.editableCategory.id = payload.id;
            state.editableCategory.parent_id = payload.parent_id;
            state.editableCategory.title = payload.title;
        },
        setCategories(state, categories) {
            state.categories = categories;
        },
        resetEditableCategory(state) {
            state.editableCategory.id = null;
            state.editableCategory.parent_id = null;
            state.editableCategory.title = null;
        }
    },
    getters: {
        categories(state) {
            return state.categories;
        },
        editableCategory(state) {
            return state.editableCategory;
        }
    }
}
