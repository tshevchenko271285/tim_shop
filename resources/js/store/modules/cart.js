export default {
    state: {
        items: [],
        visibleCart: false,
    },
    actions: {
        async getCartItems(context) {
            axios.get('/cart/getCartItemsJson').then((response)=>{
                context.commit('setCartItems', response.data );
            });
        },
        async changeCountItems(context, data) {
            let itemData = { index: null, count: data.count },
                currentItem = context.state.items.filter( (item, index) => {
                if( item.product.id == data.item.product.id ) {
                    itemData.index = index;
                    return true;
                }
            });

            context.commit('changeCountItems', itemData);``
            axios({
                method: 'put',
                url: `/cart/${currentItem[0].product.id}/changeCartItemCount`,
                data: {
                    count: data.count,
                    view: false,
                    totalCount: true,
                }
            });
        }
    },
    mutations: {
        setCartItems(state, cart) {
            state.items = cart;
        },
        toggleVisibilityCart(state) {
            state.visibleCart = !state.visibleCart;
        },
        changeCountItems(state, data) {
            state.items[data.index].count = data.count;
        }
    },
    getters: {
        getCartItemsUrl(state) {
            return state.getCartItemsUrl;
        },
        getCartItems(state){
            return state.items;
        },
        getAmountCartItems(state) {
            let count = 0;
            for( let item of state.items ) {
                count += 1*item.count;
            }
            return count;
        },
        visibilityCart(state) {
            return state.visibleCart;
        }
    },
}
