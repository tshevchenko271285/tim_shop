export default {
    state: {
        orders: [],
        pagination: {},
        selectedPage: 1,
        status: 1,
        statusNames: {
            1: 'Новый',
            2: 'Отменен',
            3: 'Не подтвержден',
            4: 'Подтвержден',
            5: 'Отправлен',
            6: 'Доставлен',
            7: 'Возврат',
            8: 'Оплачен',
        },
    },
    actions: {
        setOrders(context/*, params*/) {
            // console.log(context.state.status);
            // console.log(context.state.pagination.current_page);
            // let routeUrl = url ? url : `orders/getAllOrdersJson`;
            let params = {
                page: context.state.selectedPage,
                status: context.state.status,
            };
            axios.get('orders/getAllOrdersJson', {params})
            .then( response => {
                context.commit('setOrders', response.data.data);
                delete response.data.data;
                context.commit('setPagination', response.data);
            } );
        },
        updateOrder(context, order) {
            axios.put(`orders/${order.id}`, {order})
            .then( result => context.commit('updateOrder', result.data) );
        },
    },
    mutations: {
        setOrders(state, orders) {
            state.orders = orders;
        },
        setPagination(state, pagination) {
            state.pagination = pagination;
        },
        changeOrderStatus(state, data) {
            for( let i in state.orders ) {
                if( state.orders[i].id == data.id ) {
                    state.orders[i].status = data.status;
                    this.dispatch('updateOrder', state.orders[i]);
                    break;
                }
            }
        },
        updateOrder(state, order) {
            for( let i in state.orders ) {
                if( state.orders[i].id == order.id ) {
                    Object.assign(state.orders[i], order);
                    break;
                }
            }
        },
        changeSelectedPage(state, page) {
            state.selectedPage = page;
        },
        changeStatus(state, status) {
            state.status = status;
            this.commit('changeSelectedPage', 1);
        }
    },
    getters: {
        getOrders(state) {
            return state.orders;
        },
        getPagination(state) {
            return state.pagination;
        },
        selectedStatus(state) {
            return state.status;
        },
        statusNames(state) {
            return state.statusNames;
        }
    }
}
