export default {
    state: {
        products: [],
        pagination: {},
    },
    actions: {
        getProducts(context) {
            axios({
                method: 'POST',
                url: '/admin/products/getProducts',
            })
            .then(response => {
                context.commit('setProducts', response.data.data);
                delete response.data.data;
                context.commit('setPagination', response.data);
            });
        },
        saveProduct(context, product) {
            axios({
                method: 'PUT',
                url: '/admin/products/' + product.id,
                data: { product },
            })
            .then(response => {
                console.log(response)
            });
        },
    },
    mutations: {
        setProducts(state, products) {
            console.log(products);
            state.products = products;
        },
        setPagination(state, pagination) {
            // console.log(pagination);
            state.pagination = pagination;
        },
    },
    getters: {
        products(state) {
            return state.products;
        },
    }
}
