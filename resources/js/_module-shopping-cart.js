
$(function() {
    $('.shopping-cart').on('click', function () {
        let $this = $(this),
            routeUrl = $this.attr('data-get-cart-items-url');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: routeUrl,
            type: "GET",
        }).done(function (res) {
            if( res && res.length && $(res).hasClass('cart-window_content') ) {
                $('.cart-window_content').replaceWith(res);
                $('.cart').fadeIn(100);
            }
        });
    });
    $(document).on('click', '.cart-substrate, .cart-window_header-close', function(){
        $('.cart').fadeOut(100);
    });

    $(document).on('click', '.cart-product_delete', function(){
        let $this = $(this),
            routeUrl = $this.attr('data-delete-cart-item-url');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: routeUrl,
            type: "DELETE",
        }).done(function (res) {
            if( res && res.length && $(res).hasClass('cart-window_content') ) {
                $('.cart-window_content').replaceWith(res);
                setTotalAmount();
                $('.cart').fadeIn(100);
            }
        });
    });
    $('.product-detail_button-buy').on('click', function(){
        let $this = $(this),
            routeUrl = $this.attr('data-add-cart-url'),
            productId = $this.attr('data-product-id');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                productId: productId
            },
            url: routeUrl,
            type: "POST",
        }).done(function (res) {
            if( res && res.length && $(res).hasClass('cart-window_content') ) {
                $('.cart-window_content').replaceWith(res);
                setTotalAmount();
            }
        });
    });
    let productCountTimeout;
    $(document).on('input', '.cart-product_content-count-input', function(){
        clearTimeout(productCountTimeout);
        productCountTimeout = setTimeout(() => {
            let $this = $(this),
                routeUrl = $this.attr('data-change-cart-item-count-url'),
                count = $this.val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {count: count},
                url: routeUrl,
                type: "PUT",
            }).done(function (res) {
                if( res && res.length && $(res).hasClass('cart-window_content') ) {
                    $('.cart-window_content').replaceWith(res);
                    setTotalAmount();
                }
            });
        }, 500);
    });

    $(document).on('click', '.cart-product_content-count-button', function(){
        let $this = $(this),
            $value = $this.siblings('.cart-product_content-count-value'),
            $countInput = $this.siblings('.cart-product_content-count-input'),
            count = 1*$countInput.val();
        if( $this.hasClass('cart-product_content-count-button__plus') ) {
            count++;
        } else {
            count--;
        }
        if( count <= 0 ) return;
        $countInput.val(count).trigger('input');
        $value.text(count);
        setTotalAmount();
    });

    function setTotalAmount() {
        let count = 0;
        $('.cart-window_content').find('.cart-product_content-count-input').each(function(i, el){
            count += (1*$(el).val())
        });
        if( count ) {
            $('.nav-link.shopping-cart').attr('data-cart-items-count', count);
        } else {
            $('.nav-link.shopping-cart').removeAttr('data-cart-items-count');
        }
    }
});
