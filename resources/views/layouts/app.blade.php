<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('headers.head')
</head>
<body>
    <div id="app">

        @include('headers.top-main-menu')

        <main class="py-4">@yield('content')</main>

        @component('modals.cart')@endcomponent

    </div>

</body>
</html>
