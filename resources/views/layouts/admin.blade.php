<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('headers.head')
</head>
<body class="admin-section">

    <div id="app">

        @include('admin.sidebar')

        @include('headers.top-main-menu')

        <main class="admin-content">
            @yield('content')
        </main>

    </div>

</body>
</html>
