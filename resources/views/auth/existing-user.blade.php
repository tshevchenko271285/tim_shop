@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <h2 class="text-center mb-3">Пользователь с таким e-mail, уже существует.</h2>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8 text-center">

                                <a class="btn btn-primary" href="{{ route('login') }}">Войти</a>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-primary" href="{{ route('password.request') }}">Восстановить пароль</a>
                                @endif
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
