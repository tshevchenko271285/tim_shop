<div class="cart-window_content">
    @isset($products)
        @foreach($products as $item)
            <div class="cart-product">
                <div class="cart-product_delete" data-delete-cart-item-url="{{ route('cart.deleteCartItem', $item['product']->id) }}"><i class="far fa-trash-alt"></i></div>
                <div class="cart-product_image">
                    @isset($item['product']->thumbnail)
                        <img src="{{Storage::url($item['product']->thumbnail->path)}}" alt="{{$item['product']->title}}" title="{{$item['product']->title}}" class="img-fluid">
                    @endisset
                    @empty($item['product']->thumbnail)
                        <img src="{{getDefaultProductThumbnail()}}" alt="{{$item['product']->title}}" title="{{$item['product']->title}}" class="img-fluid">
                    @endempty
                </div>
                <div class="cart-product_content">
                    <h3 class="cart-product_content-title">{{$item['product']->title}}</h3>
                    <div class="global-currency-price cart-product_content-price" data-currency="грн.">{{$item['product']->price}}</div>
                    <div class="cart-product_content-count">
                        <div class="cart-product_content-count-button cart-product_content-count-button__minus">
                            <i class="fas fa-minus"></i>
                        </div>
                        <div class="cart-product_content-count-value">{{ $item['count'] }}</div>
                        <div class="cart-product_content-count-button cart-product_content-count-button__plus">
                            <i class="fas fa-plus"></i>
                        </div>
                        <input
                            type="number"
                            step="1"
                            value="{{ $item['count'] }}"
                            class="cart-product_content-count-input"
                            data-change-cart-item-count-url="{{ route( 'cart.changeCartItemCount', $item['product']->id ) }}"
                        >
                    </div>
                    <div class="global-currency-price cart-product_content-amount-price" data-currency="грн.">
                        Сумма: {{ number_format($item['product']->price * $item['count'], 2, ',', ' ') }}
                    </div>
                </div>
            </div>
        @endforeach
    @endisset

    @empty($products)
        <h2>Ваша корзина пуста</h2>
    @endempty
</div>
