@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <admin-product-edit :id="'{{$id}}'"></admin-product-edit>
            </div>
        </div>
    </div>
@endsection
