<nav class="admin-menu">

    <ul>

        <li class="admin-menu__item @if(\Request::is('admin/categories')) admin-menu_item_active @endif">
            <a href="{{route('categories.index')}}">Категории</a>
        </li>

        <li class="admin-menu__item @if(\Request::is('admin/products')) admin-menu_item_active @endif">
            <a href="{{route('admin.products.index')}}">Товары</a>
        </li>

    </ul>

</nav>
