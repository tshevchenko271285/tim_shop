@extends('layouts.app')

@section('content')
    <div class="container">
        @auth()
            <tim-write-message></tim-write-message>
        @endauth
        @guest()
            нужно войти
        @endguest
        <tim-messages></tim-messages>
    </div>
@endsection
