@extends('layouts.app')

@section('content')
    <div class="container product-detail">
        @auth() @if(Auth::user()->role == 'admin')
            <div class="product-detail_actions">
                <div class="product-detail_action product-detail_action-edit"><i class="fas fa-pencil-alt"></i></div>
                <div class="product-detail_action product-detail_action-save product-detail_action__hidden"><i class="far fa-save"></i></div>
            </div>
        @endif @endauth
        <div class="row">
            <div class="col-4">
                <div class="product-detail_thumbnail" data-default-thumbnail="{{ getDefaultProductThumbnail() }}">
                    <img src="{{$thumbnail}}" class="img-fluid" alt="{{$product->title}}" title="{{$product->title}}">
                </div>
            </div>
            <div class="col-8">
                <div class="product-detail_content" data-update-url="{{ route('products.update', $product->id) }}">
                    <h1 class="product-detail_content-title">{{ $product->title }}</h1>
                    <div class="product-detail_content-description">
                        <div class="product-detail_content-description-text">{!! $product->description !!}</div>
                    </div>
                    <span class="product-detail_content-price global-currency-price" data-currency="грн.">{{ $product->price }}</span>
                </div>
                <div class="product-detail_buttons">
                    <div class="button button__gray product-detail_button-buy" data-add-cart-url="{{ route('cart.addCart', $product->id) }}" @click="buyProduct">Купить</div>
                    @auth() @if(Auth::user()->role == 'admin')
                        <div class="button button__gray product-detail_button-remove" data-destroy-product-url="{{ route('products.destroy', $product->id) }}">Удалить</div>
                    @endif @endauth
                </div>
            </div>
        </div>
        <div class="owl-carousel product-gallery mt-4" data-setthumbnail-url="{{ route('products.setThumbnail', $product->id) }}">
            @auth() @if(Auth::user()->role == 'admin')
                <div id="addNewImage" class="product-gallery_item product-gallery_item__add">+</div>
            @endif @endauth
            @if( count( $gallery ) )
                @foreach( $gallery as $image )
                    <div class="product-gallery_item" data-image-id="{{ $image->id }}">
                        @auth() @if(Auth::user()->role == 'admin')
                            <div class="product-gallery_item-actions">
                                <div class="product-gallery_item-action product-gallery_item-action__thumbnail"><i class="far fa-check-circle"></i></div>
                                <div class="product-gallery_item-action product-gallery_item-action__delete"  data-delete-image="{{ route('images.destroy', $image->id) }}">
                                    <i class="far fa-trash-alt"></i>
                                </div>
                            </div>
                        @endif @endauth
                        <img src="{{ $image->path }}"  alt="{{$product->title}}" title="{{$product->title}}" class="product-gallery_item-image">
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    @auth() @if(Auth::user()->role == 'admin')
        <form action="{{ route('products.addImage', $product->id) }}" class="product-add-image-form">
            <input type="file" name="image" id="addImageField"/>
        </form>
    @endif @endauth
    <div class="container">
        @auth()
            <tim-write-message :product="{!! $product->id !!}"></tim-write-message>
        @endauth
        <tim-messages :product="{!! $product->id !!}"></tim-messages>
    </div>
@endsection
