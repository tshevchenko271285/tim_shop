@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data" class="col-lg-12">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label for="productTitle">
                        Заголовок
                        @error('title')
                            <span class="label__error">{{ $message }}</span>
                        @enderror
                    </label>
                    <input id="productTitle" name="title" value="{{ old('title') }}" type="text" class="form-control @error('title') input__error @enderror" placeholder="Enter Title">
                </div>
                <div class="form-group col-lg-6">
                    <label for="productPrice">
                        Цена
                        @error('price')
                            <span class="label__error">{{ $message }}</span>
                        @enderror
                    </label>
                    <input id="productPrice" name="price" value="{{ old('price') }}" type="number" step="0.01" class="form-control @error('price') input__error @enderror" placeholder="Enter Price">
                </div>
                <div class="form-group col-lg-12">
                    <label for="productDiscription">Описание</label>
                    <textarea id="productDiscription" name="description" value="{{ old('description') }}" class="form-control" placeholder="Enter Description"></textarea>
                </div>

                <div class="form-group col-lg-6">
                    <label for="productImage">Изображение: </label>
                    <input id="productImage" name="image" value="{{ old('image') }}" type="file">
                </div>
                <div class="form-group col-lg-6 text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            @csrf
        </form>
    </div>
</div>
@endsection
