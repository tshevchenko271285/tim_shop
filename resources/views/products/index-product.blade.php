@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row products-list">
            @foreach($products as $product)
                @auth() @if( Auth::user()->role == 'Admin' )
                    <a href="{{route('products.edit', $product->id)}}" class="col-6 col-md-3 products-list-item">
                @else
                    <a href="{{route('products.show', $product->id)}}" class="col-6 col-md-3 products-list-item">
                @endif @endauth
                @guest()
                    <a href="{{route('products.show', $product->id)}}" class="col-6 col-md-3 products-list-item">
                @endguest
                    <?php
                        $product_image = '';
                        $product_thumbnail = $product->thumbnail;
                        if( $product_thumbnail ) {
                            $product_image = $product_thumbnail->path;
                        } else {
                            $product_image = 'products/default.png';
                        }
                    ?>
                    <div class="products-list-item_image"><img class="img-fluid" src="{{ $product_image }}" alt=""></div>
                    <div class="products-list-item_price global-currency-price" data-currency="грн.">{{$product->price}}</div>
                    <div class="products-list-item_title"><h4>{{$product->title}}</h4></div>
                </a>
            @endforeach
        </div>
        <div class="row">
            <div class="col-12">
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection
