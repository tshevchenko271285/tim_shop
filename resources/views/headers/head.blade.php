
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="googlebot" content="noindex,nofollow">
<meta name="yandex" content="noindex,nofollow">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base-url" content="{{ URL::to('/') }}">

<title>{{ config('app.name', 'Laravel') }}</title>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
