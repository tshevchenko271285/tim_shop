@extends('layouts.app')

@section('content')
    <section class="container checkout-cart">
        <div class="row">
            <div class="col-lg-6">
                <div class="checkout-cart-order-items">
                    <order-items></order-items>
                </div>
            </div>
            <div class="col-lg-6">
{{--                <form action="{{ route('checkout.store') }}" method="POST" class="customer-details">--}}
{{--                    @csrf--}}
{{--                    <div class="form-group @error('name') is-invalid @enderror">--}}
{{--                        <label for="customerName">Имя</label>--}}
{{--                        <input type="text" class="form-control" id="customerName" name="name" value="{{ old('name') }}">--}}
{{--                    </div>--}}

{{--                    <div class="form-group @error('phone') is-invalid @enderror">--}}
{{--                        <label for="customerPhone">Телефон</label>--}}
{{--                        <input type="tel" class="form-control" id="customerPhone" name="phone" value="{{ old('phone') }}">--}}
{{--                    </div>--}}

{{--                    <div class="form-group @error('email') is-invalid @enderror">--}}
{{--                        <label for="customerEmail">E-mail</label>--}}
{{--                        <input type="email" class="form-control" id="customerEmail" name="email" value="{{ old('email') }}">--}}
{{--                    </div>--}}

{{--                    <div class="form-group @error('city') is-invalid @enderror">--}}
{{--                        <label for="customerCity">Город</label>--}}
{{--                        <input type="text" class="form-control" id="customerCity" name="city" value="{{ old('city') }}">--}}
{{--                    </div>--}}

{{--                    <div class="form-group @error('address') is-invalid @enderror">--}}
{{--                        <label for="customerAddress">Адрес</label>--}}
{{--                        <input type="text" class="form-control" id="customerAddress" name="address" value="{{ old('address') }}">--}}
{{--                    </div>--}}

{{--                    <div class="new-post-container" id="newPostContainer"></div>--}}
{{--                    <input type="submit" value="Оформить" class="button button__gray">--}}

{{--                </form>--}}
                <customer-details-form></customer-details-form>
            </div>
        </div>
    </section>
@endsection
