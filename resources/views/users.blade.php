@extends('layouts.app')

@section('sidebar')

    @parent

    <p>Sidebar</p>

@endsection

@section('content')
    <?php dump( $users );?>
    <h1>Users</h1>

    <form action="/users/create" method="get">
        <div class="row">
            <div class="form-group col-lg-6">
                <label for="exampleInputEmail1">Email address</label>
                <input name="name" value="{{ old('name') }}" type="text" class="form-control" placeholder="Enter email">
            </div>
            <div class="form-group col-lg-6">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" value="{{ old('password') }}" type="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group col-lg-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        @csrf
    </form>
@endsection

