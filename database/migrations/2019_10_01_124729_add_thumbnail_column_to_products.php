<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThumbnailColumnToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->bigInteger('thumbnail_id')->unsigned()->nullable()->after('price');
            $table->foreign('thumbnail_id', 'product_thumbnail_foreign_index')
                  ->references('id')
                  ->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('product_thumbnail_foreign_index');
            $table->dropColumn('thumbnail_id');
        });
    }
}
