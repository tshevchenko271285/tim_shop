<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOrdersItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_item', function (Blueprint $table) {
            $table->foreign('order_id')
                  ->references('id')->on('orders');
            $table->foreign('product_id')
                  ->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_item', function (Blueprint $table) {
            $table->dropForeign('orders_item_order_id_foreign');
            $table->dropForeign('orders_item_product_id_foreign');
        });
    }
}
