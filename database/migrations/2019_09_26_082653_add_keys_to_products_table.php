<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
//            $table->integer('image_id')->unsigned()->change();
            $table->foreign('image_id', 'products_foreign_images_id')->references('id')->on('images');
//            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id', 'products_foreign_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_foreign_images_id');
            $table->dropForeign('products_foreign_user_id');
        });
    }
}
