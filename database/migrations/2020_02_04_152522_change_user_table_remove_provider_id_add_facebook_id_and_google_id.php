<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserTableRemoveProviderIdAddFacebookIdAndGoogleId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('provider_id');

            $table->string('facebook_id')->after('email')->unique()->nullable();
            $table->string('google_id')->after('email')->unique()->nullable();
            $table->string('password')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropUnique('users_facebook_id_unique');
            $table->dropUnique('users_google_id_unique');

            $table->dropColumn('facebook_id');
            $table->dropColumn('google_id');

            $table->string('provider_id')->nullable()->after('email');
        });
    }
}
