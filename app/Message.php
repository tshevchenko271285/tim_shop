<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = ['parent_id', 'user_id', 'text', 'product_id'];

    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function children() {
        return $this->hasMany($this, 'parent_id');
    }

}
