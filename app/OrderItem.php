<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'orders_item';

    protected $fillable = ['id', 'order_id', 'product_id', 'quanity', 'sale'];

    protected $attributes = [
        'sale' => 0,
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }


}
