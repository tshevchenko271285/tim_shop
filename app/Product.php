<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    public $fillable = ['id', 'title', 'description', 'price', 'user_id'];

//    public function getHelloAttribute(){
//        return $this->title . ' - ' . $this->price;
//    }
//    public function getThumnailUrl(){
//        $thumbnail = $this->thumbnail();
//        return Storage::url($thumbnail->path);
//    }

    public function thumbnail()
    {
        return $this->hasOne('App\Image', 'id', 'thumbnail_id');
    }

    public function images()
    {
        return $this->hasMany('App\Image')->orderBy('id', 'desc');
    }

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function messages() {
        return $this->hasMany('App\Message');
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'category_product');
    }

}
