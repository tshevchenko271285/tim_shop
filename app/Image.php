<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = ['filename', 'path', 'product_id'];

    public function getPathAttribute ($value) {
        return Storage::url($value);
    }

    public function product()
    {
        return $this->belongsTo('App\Product')->withDefault(function($image, $product){
            $image->path = 'products/default.png';
            $image->filename = 'default.png';
        });
    }
}
