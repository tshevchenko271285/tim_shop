<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title', 'parent_id'];

    public function products() {
        return $this->belongsToMany('App\Product', 'category_product');
    }

    public function children() {
        return $this->hasMany($this, 'parent_id')->orderBy('title', 'asc');
    }
}
