<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;
use App\Image as Image;
use App\Product as Product;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Collection;
use App\Http\Middleware\CheckRole;
use Illuminate\Support\Arr;


class ProductController extends Controller
{
    public function __construct(Request $request) {
//        if(
//            $request->route()->named('products.setThumbnail')
//            || $request->route()->named('products.addImage')
//            || $request->route()->named('products.destroy')
//            || $request->route()->named('products.update')
//            || $request->route()->named('products.edit')
//            || $request->route()->named('products.store')
//            || $request->route()->named('products.create')
//        ) {
//            $this->middleware(['auth']);
//        }

    }


    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')
            ->with('thumbnail')
            ->paginate(8);
        return view('products.index-product', ['products' => $products ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create-product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'min:3'],
            'price' => ['required'],
        ]);
        $product = new Product([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'user_id' => Auth::id(),
        ]);
        $product->save();

        if ($request->hasFile('image')) {
            $image = $product->thumbnail()->create([
                'filename' => $request->image->hashName(),
                'path' => $request->image->store('public/products'),
                'product_id' => $product->id
            ]);
            $product->thumbnail_id = $image->id;
            $product->save();
        }
        return redirect()->route('products.edit', $product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with(['thumbnail', 'images'])->find($id);
        $thumbnail_obj = $product->thumbnail;
        $thumbnail_url = $thumbnail_obj ? $thumbnail_obj->path : Storage::url('products/default.png');
        $data = [
            'product' => $product,
            'thumbnail' => $thumbnail_url,
            'gallery' => $product->images,
        ];
        return view('products.edit-product', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $product = Product::find($id)->with(['thumbnail', 'images']);
        $product = Product::with(['thumbnail', 'images'])->find($id);
        $thumbnail_obj = $product->thumbnail;
        $thumbnail_url = $thumbnail_obj ? $thumbnail_obj->path : Storage::url('products/default.png');
        $data = [
            'product' => $product,
            'thumbnail' => $thumbnail_url,
            'gallery' => $product->images,
        ];
        return view('products.edit-product', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $request->validate([
            'title' => ['required'],
            'price' => ['required', 'numeric']
        ]);

        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        return $id;
        Product::find($id)->delete();
        return route('products.index');
    }

    public function addImage(Request $request, $id) {
        $request->validate([
           'image' => 'image'
        ]);
        $image_name = $request->image->hashName();
        $image_path = $request->file('image')->store('public/products');

        $product = Product::find($id);

        $image = $product->images()->create([
            'filename' => $image_name,
            'path' => $image_path
        ]);

        $html = '<div class="product-gallery_item" data-image-id="'.$image->id.'">';
        $html .= '<div class="product-gallery_item-actions">';
        $html .= '<div class="product-gallery_item-action product-gallery_item-action__thumbnail"><i class="far fa-check-circle"></i></div>';
        $html .= '<div class="product-gallery_item-action product-gallery_item-action__delete"  data-delete-image="'.route('images.destroy', $image->id).'">';
        $html .= '<i class="far fa-trash-alt"></i>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<img src="'.$image->path.'"  alt="'.$image->product->title.'" title="'.$image->product->title.'" class="product-gallery_item-image">';
        $html .= '</div>';

        return $html;
    }

    public function setThumbnail(Request $request, $id){
        $thumbnail_id = $request->input('image_id');
        $product = Product::find($id);
        $product->thumbnail_id = $thumbnail_id;
        $product->save();
        return 'true';
    }


}
