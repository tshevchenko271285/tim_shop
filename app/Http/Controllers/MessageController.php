<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->ajax() ) {
            $query = Message::query();
            $query->with(['user'])
                    ->where('parent_id', null)
                    ->where('product_id', $request->input('product_id'))
                    ->orderBy('id', 'desc');
            $messages = $query->paginate(5);
            return $this->allowedEditMessages($messages);
        }
//        return view('messages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !Auth::check() ){
            return response('Unauthorized', 401);
        }

        $user = Auth::user();
        $message_data = [];
        $message_data['text'] = $request->input('text');
        $message_data['product_id'] = $request->input('product_id');

        if( $request->has('parent_id') ) {
            $message_data['parent_id'] = $request->input('parent_id');
        }

        $saved_message = $user->messages()->create($message_data);
        $message = Message::with(['user'])->find($saved_message->id);
        return $this->allowedEditMessage($message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( !Auth::check() ){
            return response('Unauthorized', 401);
        }
        $message = Message::find($id);
        $message->text = $request->input('text');
        $message->save();
        return $this->allowedEditMessage($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getChildrenMessages(Request $request) {
        $parent_id = $request->input('id');
        $query = Message::query();
        $messages = $query->with(['user'])->where('parent_id', $parent_id)->paginate(5);
        return $this->allowedEditMessages($messages);
    }

    private function allowedEditMessages($messages) {
        $messages->transform(function ($message, $key) {
            return $this->allowedEditMessage($message);
        });
        return $messages;
    }

    private function allowedEditMessage($message) {
        $allow_edit = null;
        if( Auth::check() ) {
            $allow_edit = false;
        }
        if( $message->user_id === Auth::id() ) {
            $allow_edit = true;
        }
        $message->allow_edit = $allow_edit;
        return $message;
    }
}
