<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $google_user = Socialite::driver('google')->user();

        $user_by_provider = User::where('facebook_id', $google_user->id)->first();

        if( $user_by_provider ) {

            $auth_user = $user_by_provider;

        } else {

            $user_by_email = User::where('email', $google_user->email)->first();
            if( $user_by_email ) {
                $user_by_email->google_id = $google_user->id;
                $user_by_email->save();
                $auth_user = $user_by_email;
            } else {
                $user_by_provider = User::create([
                    'name' => $google_user->name,
                    'email' => $google_user->email,
                    'role' => 'user',
                    'facebook_id' => $google_user->id,
                    'password' => null,
                ]);
                $auth_user = $user_by_provider;

            }

        }
        Auth::login($auth_user);
        return redirect('products');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleFacebookProviderCallback()
    {
        $facebook_user = Socialite::driver('facebook')->user();
        $user_by_provider = User::where('facebook_id', $facebook_user->id)->first();

        if( $user_by_provider ) {

            $auth_user = $user_by_provider;

        } else {

            $user_by_email = User::where('email', $facebook_user->email)->first();
            if( $user_by_email ) {
                $user_by_email->facebook_id = $facebook_user->id;
                $user_by_email->save();
                $auth_user = $user_by_email;
            } else {
                $user_by_provider = User::create([
                    'name' => $facebook_user->name,
                    'email' => $facebook_user->email,
                    'role' => 'user',
                    'facebook_id' => $facebook_user->id,
                    'password' => null,
                ]);
                $auth_user = $user_by_provider;

            }

        }
        Auth::login($auth_user);
        return redirect('products');
    }
}
