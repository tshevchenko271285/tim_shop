<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Order;
use App\OrderItem;
use App\Customer;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( !Auth::check() ) {
            return redirect(route('products.index'));
        }
//        $orders = Order::with(['customer', 'orderItems', 'orderItems.product', 'orderItems.product.thumbnail'])->get();
//        dd($orders);
        return view('orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new_data = $request->input('order');
        $order = Order::with([
            'customer',
            'orderItems',
            'orderItems.product',
            'orderItems.product.thumbnail'
        ])->find($id);

        $order->status = $new_data['status'];

        $order->update();
        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllOrdersJson(Request $request) {
        $query = Order::query();

        if( $request->has('status') ) {
            $query->where('status', $request->input('status'));
        }

        $query->with([
            'customer',
            'orderItems',
            'orderItems.product',
            'orderItems.product.thumbnail'
        ]);
        $query->orderByDesc('id');
        $orders = $query->paginate(5);

        return $orders;
    }

}
