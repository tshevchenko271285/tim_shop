<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

use App\Customer;
use App\Order;
use App\OrderItem;

class CheckoutOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('checkout.index', ['order' => session('order')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('checkout.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'city' => 'required',
            'address' => 'required',
        ]);

        $customer = new Customer([
            'user_id' => null,
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
        ]);
        if( Auth::check() ) {
            $customer->user_id =  Auth::id();
        }
        $customer->save();

        $order = Order::create([
            'status' => 1,
            'customer_id' => $customer->id
        ]);
        $customer->orders()->save($order);

        $cart = session('cart');
        $order_items = [];
        foreach ( $cart as $item ) {
            $order_items[] = new OrderItem([
                'order_id' => $order->id,
                'product_id' => $item['product']->id,
                'quanity' => $item['count']
            ]);
        }
        $order->orderItems()->saveMany($order_items);

        $request->session()->forget('cart');
        $request->session()->flash('order', $order);

        return redirect()->route('checkout.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
