<?php

namespace App\Http\Controllers;

use App\Product as Product;
use Illuminate\Http\Request;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class CartController extends Controller
{
    public function addCart(Request $request, $id){
//        $request->session()->forget('cart'); // Очистить корзину
        $cart = $request->session()->get('cart', collect( [] ) );
        if( !$cart->contains('product.id', $id) ) {
            $product = Product::with('thumbnail')->find($id);
            $cart->push(['product' => $product, 'count' => 1]);
        }
        $request->session()->put( 'cart', $cart );
//        return view('modals.cart-content', ['products' => $cart]);
    }

    public function getCartItems(Request $request) {
        $cart = collect( $request->session()->get('cart') );
        return view( 'modals.cart-content', [ 'products' => $cart ] );
    }

    public function getCartItemsJson(Request $request) {
        $cart = collect( $request->session()->get('cart') );
        return $cart;
    }

    public function deleteCartItem( Request $request, $id ) {
        $cart = collect( $request->session()->get('cart', []) );
        $new_cart = $cart->filter(function($value, $key) use ( $id ){
            return $value['product']->id != $id;
        });
        $request->session()->put( 'cart', $new_cart );
        return view('modals.cart-content', ['products' => $new_cart]);
    }

    public function changeCartItemCount( Request $request, $id ) {
        $count = $request->input('count');
        $cart = collect( $request->session()->get('cart') );
        $amount_price = 0;
        $new_cart = $cart->map(function($value, $key) use ($id, $count, &$amount_price) {
            $price = 0;
            if( $value['product']->id == $id )
                $value['count'] = (int)$count;
            $price = (float)$value['product']->price * (int)$value['count'];
            $amount_price += $price;
            return $value;
        });
        $request->session()->put( 'cart', $new_cart );

//        return view('modals.cart-content', ['products' => $new_cart]);

    }

    public function getControllerUrls() {
        return [
            'getCartItems' => route('cart.getCartItemsJson'),
        ];
    }
}
