<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;

class AdminCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->ajax() ) {
            return Category::where('parent_id', null)
                           ->with('children')
                           ->orderBy('title', 'asc')
                           ->get();
        } else {
            return view('admin.categories.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['title' => 'required|min:3']);

        $category = new Category;
        $category->title = $request->input('title');
        $category->slug = Str::slug( $request->input('title') );
        if( $request->has('parent_id') ) {
            $category->parent_id = $request->input('parent_id');
        }

        $category->save();
        return $category;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['title' => 'required|min:3']);

        $category = Category::find($id);
        $category->title = $request->input('title');
        $category->slug = Str::slug( $request->input('title') );
        if( $request->input('parent_id') == 0 ) {
            $category->parent_id = NULL;
        } else {
            $category->parent_id = $request->input('parent_id');
        }
        $category->save();
        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $childrenDelete = $request->input('childrenDelete');
        $category = Category::find($id);
        if( $childrenDelete ) {
            $category->children()->delete();
        } else {
            $category->children->each(function($item, $key){
                Category::find($item->id)->update(['parent_id' => null]);
            });
        }
        $category->delete();
        return response('Ok', 200);
    }

    public function validateUniqueCategoryByTitle(Request $request) {
        $id = $request->input('id');
        $slug = Str::slug($request->input('title'));

        $category = Category::where('slug', $slug)->first();
        if( $category && $id !== $category->id ) {
            return 'false';
        } else {
            return 'true';
        }
    }

    public function getCategories() {
        return Category::orderBy('title', 'asc')->get();
    }

}
