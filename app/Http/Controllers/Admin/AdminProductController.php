<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        if( $request->ajax() ) {
//            return Product::with('thumbnail')->paginate(25);
//        } else {
            return view('admin.products.index');
//        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
//        if( $request->ajax() ) {
//            return Product::with('thumbnail')->find($id);
//        } else {
            return view('admin.products.edit')->with( ['id' => $id] );
//        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new_product_data = $request->input('product');
        $product = Product::find($id);

        $product->title = $new_product_data['title'];
        $product->description = $new_product_data['description'];
        $product->price = $new_product_data['price'];
//        $product->thumbnail = $new_product_data['thumbnail']['id'];
        $product->thumbnail_id = $new_product_data['thumbnail']['id'];

        $product->save();

        $product->categories()->sync($new_product_data['categories']);

//        return $new_product_data['categories'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProducts() {
        return Product::with('thumbnail')->paginate(25);
    }

    public function getProduct(Request $request) {
        $id = $request->input('id');
        return Product::with(['thumbnail', 'images', 'categories'])->find($id);
    }

}
