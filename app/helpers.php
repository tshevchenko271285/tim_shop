<?php


function getDefaultProductThumbnail(){
    return \Illuminate\Support\Facades\Storage::url('products/default.png');
}
