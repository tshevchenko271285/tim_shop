<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['id', 'user_id', 'name', 'phone', 'email', 'city', 'address'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

//    public function orders()
//    {
//        return $this->belongsToMany('App\Order');
//    }

}
