<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/run_migrate', function(){
    $exitCode = Artisan::call('migrate');
    dd($exitCode);
});
Route::get('/rollback', function(){
    $exitCode = Artisan::call('migrate:rollback');
    dd($exitCode);
});

Route::get('/test', function(){
    dd(App\Message::with('children')->get());
});
Route::get('/', function () {
    return redirect('products');
//    return view('welcome');
})->name('welcome');
Route::get('routes', function(){
    $routes = Route::getRoutes();
    foreach ( $routes as $route ) {
        dump($route);
    }
    die();
//    return dd(Route::getRoutes());
})->name('routes');

Route::get('login/google', 'Auth\LoginController@redirectToProvider')->name('loginGoogle');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('login/facebook', 'Auth\LoginController@redirectToFacebookProvider')->name('loginFacebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookProviderCallback');

Auth::routes();
//Route::get('/users', 'UserController@index')->name('user-edit')->midleware();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('products/{id}/set_thumbnail/', 'ProductController@setThumbnail')->name('products.setThumbnail');
Route::post('products/{id}/add_images/', 'ProductController@addImage')->name('products.addImage');
Route::resource('products', 'ProductController');

Route::get('cart/getControllerUrls', 'CartController@getControllerUrls')->name('cart.getControllerUrls');
Route::post('cart/{id}/addCart/', 'CartController@addCart')->name('cart.addCart');
Route::get('cart/getCartItems/', 'CartController@getCartItems')->name('cart.getCartItems');
Route::get('cart/getCartItemsJson/', 'CartController@getCartItemsJson')->name('cart.getCartItemsJson');
Route::delete('cart/{id}/deleteCartItem/', 'CartController@deleteCartItem')->name('cart.deleteCartItem');
Route::put('cart/{id}/changeCartItemCount/', 'CartController@changeCartItemCount')->name('cart.changeCartItemCount');

Route::resource('images', 'ImageController');

Route::resource('checkout', 'CheckoutOrdersController');

Route::get('orders/getAllOrdersJson', 'OrdersController@getAllOrdersJson');
Route::resource('orders', 'OrdersController');

Route::get('messages/childrens', 'MessageController@getChildrenMessages');
Route::resource('messages', 'MessageController');

/*
 * Admin Routes
 */
Route::middleware(['auth', 'checkrole'])->prefix('admin')->group(function(){
    Route::get('/', function(){
        return view('admin.dashboard');
    });

    Route::post('products/getProducts', 'Admin\AdminProductController@getProducts');
    Route::post('products/getProduct', 'Admin\AdminProductController@getProduct');
    Route::post('products/saveCategories', 'Admin\AdminProductController@saveCategories');
    Route::resource('products', 'Admin\AdminProductController', ['as'=>'admin']);

    Route::post('categories/validateUniqueCategoryByTitle', 'Admin\AdminCategoryController@validateUniqueCategoryByTitle');
    Route::post('categories/getCategories', 'Admin\AdminCategoryController@getCategories');
    Route::resource('categories', 'Admin\AdminCategoryController');


});
